// 1. what directive is used by Node.js in loading modules it needs?
// ans: require

// 2. What Node.js module contains a method for server creation?
//ans: http module

// 3.What is the method of the http object responsible for creative a server using Node.js?
//ans: createServer()

// 4.What method of the response object allows us to set status codes and content types?
//ans: writeHead()

//5. Where will console.log() output its contents when run in Node.js?
// ans: console/terminal

// 6.What property of the request object contains the address' endpoint?
//ans: request.url

//Code

const http = require('http');

const PORT = 3000;

const server = http.createServer((request, response) => {

    if (request.url == '/login') {
        response.writeHead(200, { 'Content-Type': 'text/pain' });
        response.end('Welcome to the Login Page.');
    } else {
        response.writeHead(404, { 'Content-Type': 'text/pain' });
        response.end("I'm sorry the page you are looking for cannot be found.");
    }
}).listen(PORT)

console.log(`Server is runnig at localhost:${PORT}`)
