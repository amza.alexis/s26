/*

Client-Server Architecture
	It is a computing model wherein a server hosts, delivers and manages resources that a client consumes.

	Client is an application which creates requests from a server. A client will trigger an action and response from the server.

	Server is able to host and deliver resources requested by the client. A server can answer to multiple clients.


	Node.js
		Node.js was created because Javascript was originally created and conceptualized to be used for front-end development. Node.js is an open source environment which allows to create backend applications using JS or JavaScript.

	Why is Node.js popular?
		Performance - Node.js is one of the most performing environment for creating backend application for JS

		NPM- Node Package Manager -- it is one of the largest registry for packages. Packages are methods, functions and code that greatly helps or adds with applications.

		Familiarity - JS is one of the most popular programming languages and Node.js uses JS as its primary language.
*/


let http = require("http");
//We use the "require" directive to load Node.js modules
//a module is a software component or part of a program that contains one or more routines
//HTTP is a protocol that allows the fetching of resources such as HTML documents

http.createServer(function (request, response){

//Use the writeHead() method to:
	//Set a status code for the response - a 200 means OK
	//Set the content-type of the response as a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'});

	//This sends the response with the text content 'Hello World'
	response.end('Hello World');


}).listen(4000)
//A port is a virtual point where network connections start and end
//The server will be assigned to port 4000 via the listen(4000) method where the server will listen to any requests that are sent to it eventually communicating with our server.

//When server is running, console will print the message.
console.log('Server Running at localhost:4000');

//ctrl + c = stops the running server