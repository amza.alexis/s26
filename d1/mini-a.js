const http = require('http');

const PORT = 4000;

http.createServer(function(request, response){

    if(request.url == '/homepage'){
        response.writeHead(200,{'Content-Type': 'text/plain'})
        response.end('This is the Home Page')

    } else if(request.url == '/register'){
        response.writeHead(200,{'Content-Type': 'text/plain'})
        response.end('This is the Register Page')

    } else{
        response.writeHead(404,{'Content-Type': 'text/plain'})
        response.end('Error: Not Found')
    }

    

}).listen(PORT)

console.log(`Server is running at localhost:${PORT}.`)

//ctrl + c = stops the running server